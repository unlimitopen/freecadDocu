# freecadDocu
Dokumentation über eine "einfache" Einführung in FreeCAD.
(In dieser Dokumentation wird die Version 0.16 6712 von FreeCAD eingesetzt)

Von der Enstehung einer Idee, zu einem fertigen Objekt.<p>
Häufig stellt sich die Frage: "Wie fängt man an?".
Genau hier will ich gerne ansetzen, mit kleinen Schritten zu einem fertigen
Objekt.

In diesem Tutorial geht es um die Möglichkeit mit FreeCAD ein 3D-Objekt
zu erstellen und dann mittels Exportfunktion, diesen Export über einen 3D-Drucker
auszudrucken.

Für die Erstellung eine 3D-Objekts werden folgende Programme eingesetzt:
* FreeCAD, Erstellung des 3D-Objekts
* Cura, Erstellung des GCodes für den 3D-Drucker, angepasstes Druckerprofil

___
### Objekte nach dem Schwierigkeitsgrad:
Name | Schwierigkeitsgrad <p> (1 = easy - 3 = strong)| Bemerkung
-------- | -------- | --------
CM_Fußstift | 1 | Boolischen Algorythmen (Fusion)
CM_PencilHolder | 1 |  s.o.
CM_CableHolder | 1 | s.o.
CM_DryBox | 1 | 
CM_PapierrollenHalter | 2 | s.o.
CM_FilamentHolderV2 | 2 |
CM_WindowSensorBoard | 2 |
CM_Nuts | 2 |
CM_CorsaSonnenblende | 2-3 |
CM_LamminateDisplayHolder | 2-3 |
CM_KitchenHolder| 3 |
CM_CorsaDHutablage| 3 |

___
### Interessante Links zum Thema:

#### 3D-Objekte im STL-Format
* https://www.thingiverse.com/
* https://www.myminifactory.com/
* https://www.youmagine.com/ 

#### Filament Vertrieb
* 3D-Jake: https://www.3djake.de/
* Das Filament: https://www.dasfilament.de/
___
### Hilfen rund um FreeCad

#### Änderungen der Version 0.16 zu 0.17 beachten!
* https://github.com/FreeCAD/FreeCAD/releases
* https://github.com/FreeCAD/FreeCAD/releases/tag/0.17 # Änderungen in der Version 0.17
Bei der Migration von der Version 0.16 nach 0.17 fallen bei schon "fertigen" Zeichnungen eventuell gezeichnete Strukturen raus, diese muss man im Anschluss neuzeichnen.

#### Freecad Link, Download:
* https://www.freecadweb.org/?lang=de
* https://www.freecadweb.org/wiki/Download

#### Freecad Link, Getting_Started
* https://www.freecadweb.org/wiki/Getting_started
	
#### Freecad -Hilfe, Youtube
* https://www.youtube.com/channel/UC_9HwDkwxllq5lFGkYBIH9g
* https://www.youtube.com/user/BPLRFE
	
#### Freecad Link, Forum
* https://forum.freecadweb.org/

___
#### Hilfen rund um 3D-Drucker
* http://drucktipps3d.de/
*
